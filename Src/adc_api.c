#include "adc_api.h"

#define ARR_LEN 10

static uint8_t busy[ARR_LEN] = {0};
static uint8_t read[ARR_LEN] = {1};
static ADC_HandleTypeDef* busyAdcs[ARR_LEN] = {NULL};
static uint32_t* values[ARR_LEN] = {0};

uint32_t ADC_API_ReadValue_Sync(ADC_HandleTypeDef* hadc, uint32_t timeout)
{
	HAL_ADC_Start(hadc);
	HAL_ADC_PollForConversion(hadc, timeout);
	uint32_t adc_value = HAL_ADC_GetValue(hadc);
	HAL_ADC_Stop(hadc);
	return adc_value;
}

uint8_t ADC_API_Start_Async_Conversion(ADC_HandleTypeDef* hadc, uint32_t* val) //OVDE PROVERI STA SE DESAVA SA ASINHRONIM CITANJEM TJ STA ZNACI
{
	for(int i = 0; i < ARR_LEN;i++)
	{
		if(busyAdcs[i] == hadc)
			return 0;
	}

	for(int i = 0;i < ARR_LEN;i++)
	{
		if(busy[i] == 0)
		{
			busy[i] = 1;
			busyAdcs[i] = hadc;
			HAL_ADC_Start_IT(hadc);
			read[i] = 0;
			return 1;
		}
	}

	return 0;
}

int wait_for_ADC(ADC_HandleTypeDef* hadc)
{
	for(int i = 0; i < ARR_LEN;i++)
	{
		if(busy[i] == 1 && busyAdcs[i] == hadc)
		{
			while(read[i] == 0);
			return i;
		}
	}
	return -1;
}

uint32_t ADC_API_Read_Async(ADC_HandleTypeDef* hadc)
{
	int index = wait_for_ADC(hadc);
	if(index < 0)
		return -1;
	busy[index] = 0;
	read[index] = 0;
	busyAdcs[index] = NULL;
	return *values[index];
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	for(int i = 0; i < ARR_LEN;i++)
	{
		if(busyAdcs[i] == hadc && busy[i] == 1)
		{
			*values[i] = HAL_ADC_GetValue(hadc);
			read[i] = 1;
		}
	}
}


