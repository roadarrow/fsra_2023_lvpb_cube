#ifndef bq79606
#define bq79606

#include "ParametersConfig.h"
#include "BQ79606.h"
#include "uart_api.h"
#include "usart.h"
#include "tim.h"
#include <stdio.h>
//#include "Can.h"
//#include "canMessage.h"
//#include "FaultDetection.h"
//#include "TemperatureMonitoring.h"
//#include "SubsystemsConfig.h"
//#include "Utils.h"

BYTE recBuff[1024];
float voltageSum = 0;
uint8_t pFrame1[(MAXBYTES+6)*TOTALBOARDS];
int bRes = 0;
int count = 10000;
uint8_t pFrame[(MAXBYTES+6)*TOTALBOARDS];
BYTE bBuf[8];
BYTE bReturn = 0;
BYTE response_frame2[(MAXBYTES+6)*TOTALBOARDS];
BYTE bFrame[(2+6)*TOTALBOARDS];
int nCurrentBoard = 0;
float voltages[NoOfCells];

//NA DALJE ZAKOMENTARISANO BI TREBALO DA JE DEFINISANO U IOC
//extern DigitalOut LV_ON;
//extern BufferedSerial bms;
//extern SerialM pc1;
//extern DigitalIn bmsFault;
//extern DigitalOut bmsWakeUp;
extern char output[70];
//extern float DutyPump1; OVO MOGUCE DA JE ZA PWM IL NE NISAM SIGURNA PROVERI ZA PWM
//extern float DutyPump2;
//extern bool uvFaultMasked;
//extern bool ovFaultMasked;

void sendUART(int length, uint8_t * data) //OVA I DALJE NIJE RESENA DO KRAJA
{
#ifndef SCRYPT_TEST
	HAL_Delay(1);// MUST
#endif
#ifdef SCRYPT_TEST
    HAL_Delay(500);
#endif
    Uart_Transmit(&huart5, data, length, 1000000); //TREBA DA SE PROVERI ZA TIMEOUT
}

void Wake79606() //OVDE ISTO PROVERI STA JE SA OVIM FUNKCIJAMA TJ DA LI MOZE HAL_DELAY ZA OBE IZ MBDA DA SE KORISTE
{
    bmsWakeUp = 1;
    //HAL_Delay(0.275);
    HAL_TIM_Base_Start_IT(&htim3);
    while (!flag){};
    flag = 0;
    bmsWakeUp = 0;
    HAL_TIM_Base_Start_IT(&htim3);
    while (!flag){};
    flag = 0;
    bmsWakeUp = 1;
    HAL_Delay(10);
}

//NAREDNE 4 VALJDA MOGU DA OSTANU OVAKO SAMO PROVERITI STA TACNO RADE AL ONO PALE I GASE NAOAJANJE AKO DOBRO RAZUMEM
//VIDI DA LI WRITE_RAG RADI I OVDE APSOLUTNO NEMAM IDEJU KAKO SE RADI UPIS U REGISTRE
void ShutDownPower()
{
    ShutDown79606();
    LV_ON = 0;
}

bool IsPowerOn()
{
    return LV_ON;
}

void TurnOnPower(void)
{
    LV_ON = 1;
}

void ShutDown79606()
{
    WriteReg(0, CONTROL1, 0x08, 1, FRMWRT_ALL_NR);
}

void InitialFlush()//ZNACI OVO SLUZI DA SE OTKLONE TI NEKI POCETNI PODACI KOJI SU VEROVATNO SAMO NEKI SUM DOK SE NE UDJE PRETPOSTAVLJAM U STACIONARNO STANJE>>> PROVERI
{
    HAL_Delay(1);
    BYTE data[256];
    if(Uart_Receive(&huart5, data, sizeof(data), 100000) != HAL_OK){//TREBA FAULT NEKI NACI STA TU JOS
    	}
    }

}

//ZASTO POSTOJE DVE FJE ZA POSTAVLJANJE BAUDRATE
void SetBMSBaudrate250()
{
    WriteReg(0, COMM_CTRL, 0x34, 1, FRMWRT_ALL_NR);
    HAL_Delay(2);
    HAL_UART_DeInit(&huart5);
    huart5.Init.BaudRate = UART_BMS_BAUDRATE_NEW;
    HAL_UART_Init(&huart5);
}

/*void SetBMSBaudrate()//STA JE OVO POBOGU
{
    bms.set_baud(10);
    bms.send_break();
    bms.set_baud(250000);
}*/

void AutoAddress() //OVO JE KAO DEFINISANO ZA OVAJ BMS I TAKO TREBA DA OSTANE AKO SE NE VARAM
{
    memset(response_frame2,0,sizeof(response_frame2)); //clear out the response frame buffer

    //clear CONFIG in case it is set
    WriteReg(0, CONFIG, 0x00, 1, FRMWRT_ALL_NR);

    //enter auto addressing mode
    WriteReg(0, CONTROL1, 0x01, 1, FRMWRT_ALL_NR);

    WriteReg(0, CONFIG, 0x01, 1, FRMWRT_SGL_NR);

    //OPTIONAL: read back all device addresses
    WriteReg(0, COMM_TO, 0x04, 1, FRMWRT_ALL_NR);
    for (nCurrentBoard = 0; nCurrentBoard < TOTALBOARDS; nCurrentBoard++) {
        memset(response_frame2, 0, sizeof(response_frame2));
        ReadReg(nCurrentBoard, DEVADD_USR, response_frame2, 1, 0, FRMWRT_SGL_R);
        //printf("Board %d=%02x\n",nCurrentBoard,response_frame2[4]);
    }
}
//**************************
//END AUTO ADDRESS SEQUENCE
//**************************


//************************
//WRITE AND READ FUNCTIONS
//************************
int WriteReg(BYTE bID, uint16_t wAddr, uint64_t dwData, BYTE bLen, BYTE bWriteType) //UPIS U REGISTRE MORA OSTATI ISTI VALJDA TO OSTAVLJAM
{
    // device address, register start address, data bytes, data length, write type (single, broadcast, stack)
    bRes = 0;
    memset(bBuf,0,sizeof(bBuf));
    switch (bLen) {
    case 1:
        bBuf[0] = dwData & 0x00000000000000FF;
        bRes = WriteFrame(bID, wAddr, bBuf, 1, bWriteType);
        break;
    case 2:
        bBuf[0] = (dwData & 0x000000000000FF00) >> 8;
        bBuf[1] = dwData & 0x00000000000000FF;
        bRes = WriteFrame(bID, wAddr, bBuf, 2, bWriteType);
        break;
    case 3:
        bBuf[0] = (dwData & 0x0000000000FF0000) >> 16;
        bBuf[1] = (dwData & 0x000000000000FF00) >> 8;
        bBuf[2] = dwData & 0x00000000000000FF;
        bRes = WriteFrame(bID, wAddr, bBuf, 3, bWriteType);
        break;
    case 4:
        bBuf[0] = (dwData & 0x00000000FF000000) >> 24;
        bBuf[1] = (dwData & 0x0000000000FF0000) >> 16;
        bBuf[2] = (dwData & 0x000000000000FF00) >> 8;
        bBuf[3] = dwData & 0x00000000000000FF;
        bRes = WriteFrame(bID, wAddr, bBuf, 4, bWriteType);
        break;
    case 5:
        bBuf[0] = (dwData & 0x000000FF00000000) >> 32;
        bBuf[1] = (dwData & 0x00000000FF000000) >> 24;
        bBuf[2] = (dwData & 0x0000000000FF0000) >> 16;
        bBuf[3] = (dwData & 0x000000000000FF00) >> 8;
        bBuf[4] = dwData & 0x00000000000000FF;
        bRes = WriteFrame(bID, wAddr, bBuf, 5, bWriteType);
        break;
    case 6:
        bBuf[0] = (dwData & 0x0000FF0000000000) >> 40;
        bBuf[1] = (dwData & 0x000000FF00000000) >> 32;
        bBuf[2] = (dwData & 0x00000000FF000000) >> 24;
        bBuf[3] = (dwData & 0x0000000000FF0000) >> 16;
        bBuf[4] = (dwData & 0x000000000000FF00) >> 8;
        bBuf[5] = dwData & 0x00000000000000FF;
        bRes = WriteFrame(bID, wAddr, bBuf, 6, bWriteType);
        break;
    case 7:
        bBuf[0] = (dwData & 0x00FF000000000000) >> 48;
        bBuf[1] = (dwData & 0x0000FF0000000000) >> 40;
        bBuf[2] = (dwData & 0x000000FF00000000) >> 32;
        bBuf[3] = (dwData & 0x00000000FF000000) >> 24;
        bBuf[4] = (dwData & 0x0000000000FF0000) >> 16;
        bBuf[5] = (dwData & 0x000000000000FF00) >> 8;
        bBuf[6] = dwData & 0x00000000000000FF;
        bRes = WriteFrame(bID, wAddr, bBuf, 7, bWriteType);
        break;
    case 8:
        bBuf[0] = (dwData & 0xFF00000000000000) >> 56;
        bBuf[1] = (dwData & 0x00FF000000000000) >> 48;
        bBuf[2] = (dwData & 0x0000FF0000000000) >> 40;
        bBuf[3] = (dwData & 0x000000FF00000000) >> 32;
        bBuf[4] = (dwData & 0x00000000FF000000) >> 24;
        bBuf[5] = (dwData & 0x0000000000FF0000) >> 16;
        bBuf[6] = (dwData & 0x000000000000FF00) >> 8;
        bBuf[7] = dwData & 0x00000000000000FF;
        bRes = WriteFrame(bID, wAddr, bBuf, 8, bWriteType);
        break;
    default:
        break;
    }
    return bRes;
}

int WriteFrame(BYTE bID, uint16_t wAddr, BYTE * pData, BYTE bLen, BYTE bWriteType) {
    int bPktLen = 0;
    uint8_t * pBuf = pFrame;
    uint16_t wCRC;
    memset(pFrame, 0x7F, sizeof(pFrame));
    *pBuf++ = 0x80 | (bWriteType) | ((bWriteType & 0x10) ? bLen - 0x01 : 0x00); //Only include blen if it is a write; Writes are 0x90, 0xB0, 0xD0
    if (bWriteType == FRMWRT_SGL_R || bWriteType == FRMWRT_SGL_NR)
    {
        *pBuf++ = (bID & 0x00FF);
    }
    *pBuf++ = (wAddr & 0xFF00) >> 8;
    *pBuf++ = wAddr & 0x00FF;

    while (bLen--)
        *pBuf++ = *pData++;

    bPktLen = pBuf - pFrame;

    wCRC = CRC16(pFrame, bPktLen);
    *pBuf++ = wCRC & 0x00FF;
    *pBuf++ = (wCRC & 0xFF00) >> 8;
    bPktLen += 2;
    //THIS SEEMS to occasionally drop bytes from the frame. Sometimes is not sending the last frame of the CRC.
    //(Seems to be caused by stack overflow, so take precautions to reduce stack usage in function calls)
    //sciSend(scilinREG, bPktLen, pFrame);

    sendUART(bPktLen, pFrame);

    return bPktLen;
}

int ReadReg(BYTE bID, uint16_t wAddr, BYTE * pData, BYTE bLen, uint32_t dwTimeOut,
        BYTE bWriteType) {
    bRes = 0;
    if (bWriteType == FRMWRT_SGL_R) {
        ReadFrameReq(bID, wAddr, bLen, bWriteType);
        bRes = bLen + 6;
    } else if (bWriteType == FRMWRT_ALL_R) {
        bRes = ReadFrameReq(bID, wAddr, bLen, bWriteType);
        bRes = (bLen + 6) * TOTALBOARDS;
    } else {
        bRes = 0;
    }
    return bRes;
}

int ReadFrameReq(BYTE bID, uint16_t wAddr, BYTE bByteToReturn, BYTE bWriteType) {
    bReturn = bByteToReturn - 1;

    if (bReturn > 127)
        return 0;

    return WriteFrame(bID, wAddr, &bReturn, 1, bWriteType);
}



void Init()
{
    WriteReg(0, CONTROL2, 0x00, 1, FRMWRT_ALL_NR);
    /* mask all low level faults... user should unmask necessary faults */
    WriteReg(0, TONE_FLT_MSK, 0x07, 1, FRMWRT_ALL_NR); //mask all tone faults
    WriteReg(0, COMM_UART_FLT_MSK, 0x07, 1, FRMWRT_ALL_NR); //mask UART faults
    WriteReg(0, COMM_UART_RC_FLT_MSK, 0x1B, 1, FRMWRT_ALL_NR); //unmask IERR, BERR UART_RC faults contd
    WriteReg(0, COMM_UART_RR_FLT_MSK, 0x09, 1, FRMWRT_ALL_NR); //unmask BERR UART_RR Fault
    WriteReg(0, COMM_UART_TR_FLT_MSK, 0x03, 1, FRMWRT_ALL_NR); //mask UART_TR faults
    WriteReg(0, COMM_COMH_FLT_MSK, 0x3F, 1, FRMWRT_ALL_NR); //mask COMH faults
    WriteReg(0, COMM_COMH_RC_FLT_MSK, 0x3F, 1, FRMWRT_ALL_NR); //mask COMH_RC faults
    WriteReg(0, COMM_COMH_RR_FLT_MSK, 0x3F, 1, FRMWRT_ALL_NR); //mask COMH_RR faults
    WriteReg(0, COMM_COMH_TR_FLT_MSK, 0x03, 1, FRMWRT_ALL_NR); //mask COMH_TR faults
    WriteReg(0, COMM_COML_FLT_MSK, 0x3F, 1, FRMWRT_ALL_NR); //mask COML faults
    WriteReg(0, COMM_COML_RC_FLT_MSK, 0x3F, 1, FRMWRT_ALL_NR); //mask COML_RC faults
    WriteReg(0, COMM_COML_RR_FLT_MSK, 0x3F, 1, FRMWRT_ALL_NR); //mask COML_RR faults
    WriteReg(0, COMM_COML_TR_FLT_MSK, 0x03, 1, FRMWRT_ALL_NR); //mask COML_TR faults
    WriteReg(0, OTP_FLT_MSK, 0x07, 1, FRMWRT_ALL_NR); // mask otp faults
    WriteReg(0, RAIL_FLT_MSK, 0xFF, 1, FRMWRT_ALL_NR); //mask power rail faults
    WriteReg(0, SYSFLT1_FLT_MSK, 0x07, 1, FRMWRT_ALL_NR); //umask sys fault 1, TWARN, CTS, TSD faults
    WriteReg(0, SYSFLT2_FLT_MSK, 0xFF, 1, FRMWRT_ALL_NR); //mask sys fault 2
    WriteReg(0, SYSFLT3_FLT_MSK, 0x37, 1, FRMWRT_ALL_NR); //unmask sys fault 3, AUX_FILT, CB_VDONE faults
    WriteReg(0, UV_FLT_MSK, 0xF0, 1, FRMWRT_ALL_NR); //unmask uv fault for all cells
    WriteReg(0, OV_FLT_MSK, 0xF0, 1, FRMWRT_ALL_NR); //unmask ov fault for all cells
#if NoOfNTCs == 6
    WriteReg(0, GPIO_FLT_MSK, 0xC0, 1, FRMWRT_ALL_NR); //unmask gpio fault for all NTCs
#endif
#if NoOfNTCs == 4
    WriteReg(0, GPIO_FLT_MSK, 0xF0, 1, FRMWRT_ALL_NR); //unmask gpio fault for all NTCs
#endif
    WriteReg(0, CELL_ADC_CTRL, 0x1F, 1, FRMWRT_ALL_NR); //enables ADC for all first 4 cell channels
    WriteReg(0, OVUV_CTRL, 0x3F, 1, FRMWRT_ALL_NR); //enable all cell ov/uv
    WriteReg(0, UV_THRESH, setUnderVoltageRegister(UNDER_VOLTAGE), 1, FRMWRT_ALL_NR); //sets cell UV to 2.8V
    WriteReg(0, OV_THRESH, setOverVoltageRegister(OVER_VOLTAGE), 1, FRMWRT_ALL_NR); //sets cell OV to 4.3V

    WriteReg(0, GPIO_ADC_CONF, 0x3F, 1, FRMWRT_ALL_NR); //configure GPIO as AUX voltage (absolute voltage, set to 0 for ratiometric)

    WriteReg(0, AUX_ADC_CONF, 0x08, 1, FRMWRT_ALL_NR); //1MHz AUX sample rate,  128 decimation  ratio
    WriteReg(0, CELL_ADC_CONF1, 0x67, 1, FRMWRT_ALL_NR); //256 decimation ratio, 1MHz sample. 1.2 Hz LPF
    WriteReg(0, CELL_ADC_CONF2, 0x00, 1, FRMWRT_ALL_NR); //single conversion
    ///enable continuous sampling. Otherwise, single conversions with CONTROL2[CELL_ADC_GO]
    //WriteReg(0,CELL_ADC_CONF2, 0x0A,1,FRMWRT_ALL_NR);//continuous sampling with 5ms interval
    WriteReg(0, CONTROL2, 0x15, 1, FRMWRT_ALL_NR);// enable TSREF to give enough settling time //CELL_ADC_GO = 1 //OVUV_EN = 1
    ThisThread::sleep_for(2ms); // provides settling time for TSREF

    //WriteReg(0, DIAG_CTRL2, 0x41, 1, FRMWRT_ALL_NR); //set AUX ADC to measure  cell 1
    WriteReg(0, GPIO1_CONF, 0x20, 1, FRMWRT_ALL_NR); //configure GPIO1 as input
    WriteReg(0, GPIO2_CONF, 0x20, 1, FRMWRT_ALL_NR); //configure GPIO2 as input
    WriteReg(0, GPIO3_CONF, 0x20, 1, FRMWRT_ALL_NR); //configure GPIO3 as input
    WriteReg(0, GPIO4_CONF, 0x20, 1, FRMWRT_ALL_NR); //configure GPIO4 as input
#if NoOfNTCs == 6
    WriteReg(0, GPIO5_CONF, 0x20, 1, FRMWRT_ALL_NR); //configure GPIO5 as input
    WriteReg(0, GPIO6_CONF, 0x20, 1, FRMWRT_ALL_NR); //configure GPIO6 as input
#endif
    WriteReg(0, AUX_ADC_CTRL1, 0xF0, 1, FRMWRT_ALL_NR); //enable all GPIOs

    // Cell Balancing Init
    WriteReg(0, CB_CONFIG, setCbConfigRegister(BALANCING_DUTY_CYCLE), 1, FRMWRT_ALL_NR); // Odds then Evens, continue regardless of fault condition, 30sec, seconds

    //WriteReg(0, CB_DONE_THRESHOLD, 0x48, 1, FRMWRT_ALL_NR); // Thresh hold set to value 3.0V, CBDONE comparators enabled
    //WriteReg(0, CB_DONE_THRESHOLD, 0x68, 1, FRMWRT_ALL_NR); // Thresh hold set to value 3.8V, CBDONE comparators enabled

    WriteReg(0, CB_CELL1_CTRL, setCbCellConfigRegister(BALANCING_TIME_PER_CELL), 1, FRMWRT_ALL_NR);//cell 1- 1 minute balance timer
    WriteReg(0, CB_CELL2_CTRL, setCbCellConfigRegister(BALANCING_TIME_PER_CELL), 1, FRMWRT_ALL_NR);//cell 2- 1 minute balance timer
    WriteReg(0, CB_CELL3_CTRL, setCbCellConfigRegister(BALANCING_TIME_PER_CELL), 1, FRMWRT_ALL_NR);//cell 3- 1 minute balance timer
    WriteReg(0, CB_CELL4_CTRL, setCbCellConfigRegister(BALANCING_TIME_PER_CELL), 1, FRMWRT_ALL_NR);//cell 4- 1 minute balance timer

}

// CRC16 TABLE
// ITU_T polynomial: x^16 + x^15 + x^2 + 1
const uint16_t crc16_table[256] = { 0x0000, 0xC0C1, 0xC181, 0x0140, 0xC301,
        0x03C0, 0x0280, 0xC241, 0xC601, 0x06C0, 0x0780, 0xC741, 0x0500, 0xC5C1,
        0xC481, 0x0440, 0xCC01, 0x0CC0, 0x0D80, 0xCD41, 0x0F00, 0xCFC1, 0xCE81,
        0x0E40, 0x0A00, 0xCAC1, 0xCB81, 0x0B40, 0xC901, 0x09C0, 0x0880, 0xC841,
        0xD801, 0x18C0, 0x1980, 0xD941, 0x1B00, 0xDBC1, 0xDA81, 0x1A40, 0x1E00,
        0xDEC1, 0xDF81, 0x1F40, 0xDD01, 0x1DC0, 0x1C80, 0xDC41, 0x1400, 0xD4C1,
        0xD581, 0x1540, 0xD701, 0x17C0, 0x1680, 0xD641, 0xD201, 0x12C0, 0x1380,
        0xD341, 0x1100, 0xD1C1, 0xD081, 0x1040, 0xF001, 0x30C0, 0x3180, 0xF141,
        0x3300, 0xF3C1, 0xF281, 0x3240, 0x3600, 0xF6C1, 0xF781, 0x3740, 0xF501,
        0x35C0, 0x3480, 0xF441, 0x3C00, 0xFCC1, 0xFD81, 0x3D40, 0xFF01, 0x3FC0,
        0x3E80, 0xFE41, 0xFA01, 0x3AC0, 0x3B80, 0xFB41, 0x3900, 0xF9C1, 0xF881,
        0x3840, 0x2800, 0xE8C1, 0xE981, 0x2940, 0xEB01, 0x2BC0, 0x2A80, 0xEA41,
        0xEE01, 0x2EC0, 0x2F80, 0xEF41, 0x2D00, 0xEDC1, 0xEC81, 0x2C40, 0xE401,
        0x24C0, 0x2580, 0xE541, 0x2700, 0xE7C1, 0xE681, 0x2640, 0x2200, 0xE2C1,
        0xE381, 0x2340, 0xE101, 0x21C0, 0x2080, 0xE041, 0xA001, 0x60C0, 0x6180,
        0xA141, 0x6300, 0xA3C1, 0xA281, 0x6240, 0x6600, 0xA6C1, 0xA781, 0x6740,
        0xA501, 0x65C0, 0x6480, 0xA441, 0x6C00, 0xACC1, 0xAD81, 0x6D40, 0xAF01,
        0x6FC0, 0x6E80, 0xAE41, 0xAA01, 0x6AC0, 0x6B80, 0xAB41, 0x6900, 0xA9C1,
        0xA881, 0x6840, 0x7800, 0xB8C1, 0xB981, 0x7940, 0xBB01, 0x7BC0, 0x7A80,
        0xBA41, 0xBE01, 0x7EC0, 0x7F80, 0xBF41, 0x7D00, 0xBDC1, 0xBC81, 0x7C40,
        0xB401, 0x74C0, 0x7580, 0xB541, 0x7700, 0xB7C1, 0xB681, 0x7640, 0x7200,
        0xB2C1, 0xB381, 0x7340, 0xB101, 0x71C0, 0x7080, 0xB041, 0x5000, 0x90C1,
        0x9181, 0x5140, 0x9301, 0x53C0, 0x5280, 0x9241, 0x9601, 0x56C0, 0x5780,
        0x9741, 0x5500, 0x95C1, 0x9481, 0x5440, 0x9C01, 0x5CC0, 0x5D80, 0x9D41,
        0x5F00, 0x9FC1, 0x9E81, 0x5E40, 0x5A00, 0x9AC1, 0x9B81, 0x5B40, 0x9901,
        0x59C0, 0x5880, 0x9841, 0x8801, 0x48C0, 0x4980, 0x8941, 0x4B00, 0x8BC1,
        0x8A81, 0x4A40, 0x4E00, 0x8EC1, 0x8F81, 0x4F40, 0x8D01, 0x4DC0, 0x4C80,
        0x8C41, 0x4400, 0x84C1, 0x8581, 0x4540, 0x8701, 0x47C0, 0x4680, 0x8641,
        0x8201, 0x42C0, 0x4380, 0x8341, 0x4100, 0x81C1, 0x8081, 0x4040 };

uint16_t CRC16(BYTE *pBuf, int nLen)
{
    uint16_t wCRC = 0xFFFF;
    int i;

    for (i = 0; i < nLen; i++)
    {
        wCRC ^= (*pBuf++) & 0x00FF;
        wCRC = crc16_table[wCRC & 0x00FF] ^ (wCRC >> 8);
    }

    return wCRC;
}

BYTE WaitRegRead()
{
#ifndef SCRYPT_TEST
    HAL_Delay(1);
#endif
#ifdef SCRYPT_TEST
    HAL_Delay(500);
#endif
    if (!CheckUARTConnection())
    {
        return 0x00;
    } //OVDE PROBLEM OVA FJA U IF PROVERAVA DA LI RADI UART I ONDA VRACA TRUE/FALSE ALI KORISTI BMS READABLE A MI TO OVDE NEMAMO I SVAKAKO ZOVE HENDLER AKO
    //NE RADI JA GA SVK JOS NEMAM ALI NACI DA LI OVDE POSTOJI NEKA FUNKCIJA KOJA SAMO VRACA STANJE UARTA IL OPET DA ZOVEM OVU FJU DOLE PA KAO STA ONA VRATI I AKO DA DA LI TO DA SE SASTAVI U NEKU ODVOJENU FJU

    Uart_Recieve(&huart5, recBuff, 8, 1000000);//NADJI ZA TIMEOUT
    return recBuff[4];
}

void WaitFrame(int noBytes, int noCell)//CEMU SLUZI OVA FJA
{
    HAL_DELAY(10);
    if (!CheckUARTConnection())//OPET ZA OVO VIDI STA CEMO
    {
        return;
    }
    Uart_Receive(&huart5, recBuff, noBytes, 1000000);

#ifdef SERIAL_HUMAN_READABLE
    Uart_Transmit(&huart4, "\n ", sizeof("\n "), 1000000);
#endif
    int voltage = recBuff[5];   //LSB
    voltage |= (recBuff[4]) << 8; //MSB
    float vol = voltage*0.0001907349;//BILO BI VRH KAD BI MI NEKO OBJASNIO OVO IL NADJI SAMA
#ifdef SERIAL_HUMAN_READABLE
    char pcOut[50];
    sprintf(pcOut, "Cell[%d] = %f V          ", noCell, vol);
    Uart_Transmit(&huart4, pcOut, sizeof(pcOut), 1000000);
#endif
    voltageSum = voltageSum + vol;
    VOLTAGE_INFO[noCell - 1] = recBuff[4];
    SetVoltageFaults(noCell, vol);
    voltages[noCell - 1] = vol; //TAKODJE OVDE NEMAM POJMA STA SE DESAVA
}

void WaitFrameTemp(int noBytes, int noGPIO) //OVA ISTO NE ZNAM CEMU SLUZI
{
    HAL_DELAY(10);
    if (!CheckUARTConnection())
    {
        return;
    } //OVDE ISTO VIDI STA CEMO SA OVIM
    Uart_Receive(&huart5, recBuff, noBytes, 1000000);

    int temp = 0;

    uint16_t voltage = recBuff[5]; // LSB
    voltage |= (recBuff[4]) << 8; // MSB
    double Vout = voltage*0.0001907349;

    if (CheckNTCConnection(noGPIO, Vout))//OVO ISTO KAO ZA UART VIDI STA CEMO
    {
        temp = VoltageDivider(Vout);
        TEMPERATURE_INFO[noGPIO - 1] = temp;
#ifdef SERIAL_HUMAN_READABLE
        char pcOut[50]; //MOZDA DA NAPRAVIS FJU ZA OVO VISE PUTA SE KORISTI
        sprintf(pcOut, "Temp[%d] = %d C          ", noGPIO, temp);
        Uart_Transmit(&huart4, pcOut, sizeof(pcOut), 1000000);
#endif
        SetTempFaults(noGPIO, temp);//MORAS DA SREDIS OVE FOLTOVE
    }
    else
    {
        temp = 30;
        TEMPERATURE_INFO[noGPIO - 1] = temp;
#ifdef SERIAL_HUMAN_READABLE
        char pcOut[50];
        sprintf(pcOut, "Temp[%d] = %d C          ", noGPIO, temp);
        Uart_Transmit(&huart4, pcOut, sizeof(pcOut), 1000000);
#endif
        SetTempFaults(noGPIO, temp);
    }
    HAL_DELAY(10);

}

void RequestTemperature() //OVDE MISLIM UPISUJE U REGISTRE TEMPERATURE
{
#ifdef SERIAL_HUMAN_READABLE
	char pcOut[] = "----------------------------TEMPERATURE-----------------------------\n";
    Uart_Transmit(&huart4, pcOut, sizeof(pcOut), 10000000);
#endif
    int noBytes = 2+6;
     WriteReg(0, CONTROL2, 0x16, 1, FRMWRT_ALL_NR);

    ReadReg(0, AUX_GPIO1H, pFrame1, 2 , 0, FRMWRT_ALL_R);
    WaitFrameTemp(noBytes,1);

    ReadReg(0, AUX_GPIO2H, pFrame1, 2 , 0, FRMWRT_ALL_R);
    WaitFrameTemp(noBytes,2);

    ReadReg(0, AUX_GPIO3H, pFrame1, 2 , 0, FRMWRT_ALL_R);
    WaitFrameTemp(noBytes,3);

    ReadReg(0, AUX_GPIO4H, pFrame1, 2 , 0, FRMWRT_ALL_R);
    WaitFrameTemp(noBytes,4);

    HAL_DELAY(10);
}

void RequestVoltage() //OVDE MISLIM UPISUJE U REGISTRE NAPONE
{
    // Posto je podesena One Shot ADC konverzija, pre citanja napona mora svaki put da se pokrene
    WriteReg(0, CONTROL2, 0x15, 1, FRMWRT_ALL_NR); //CELL_ADC_GO = 1

    int noBytes = 6+2;
#ifdef SERIAL_HUMAN_READABLE
    char pcOut[] = "---------------------- LOW VOLTAGE DIAGNOSTIC ----------------------\n";
	Uart_Transmit(&huart4, pcOut, sizeof(pcOut), 10000000);
#endif
    ReadReg(0, VCELL1H , pFrame1, 2, 0, FRMWRT_ALL_R); //6 bajtova jer cita od adrese VCELL1H po dva bajta za svaki kanal (ima 3 kanala)
    WaitFrame(noBytes,1);

    ReadReg(0, VCELL2H , pFrame1, 2, 0, FRMWRT_ALL_R); //6 bajtova jer cita od adrese VCELL1H po dva bajta za svaki kanal (ima 3 kanala)
    WaitFrame(noBytes,2);

    ReadReg(0, VCELL3H , pFrame1, 2, 0, FRMWRT_ALL_R); //6 bajtova jer cita od adrese VCELL1H po dva bajta za svaki kanal (ima 3 kanala)
    WaitFrame(noBytes,3);

    ReadReg(0, VCELL4H , pFrame1, 2, 0, FRMWRT_ALL_R); //6 bajtova jer cita od adrese VCELL1H po dva bajta za svaki kanal (ima 3 kanala)
    WaitFrame(noBytes,4);

    HAL_DELAY(10);
}

void ResetGatheredInformation() //RESETUJE SAMO INFO NISTA SPEC
{
    voltageSum = 0;
    maxTemp = 0;
}

void UartBreak()//OVA FJA ISTO NE ZNAM CEMU SLUZI AL MISLIM DA SAM JE ISPRAVILA SAMO DA SKONTAM CEMU SLUZI
{
    //Commented out since it tends to break the whole communication, Fault reset as a COMM reset seems to be enough
    //bms.send_break();
    HAL_Delay(1);
    WriteReg(0, COMM_UART_FLT_RST, 0xFF, 1, FRMWRT_ALL_NR);
    HAL_Delay(1);
}


#endif


