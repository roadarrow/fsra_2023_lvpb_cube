#include "can_api.h"
#include <string.h>
#include <stdio.h>

#define MIN_ID 0
#define MAX_ID 0x1000

typedef struct
{
	union {
		CAN_RxHeaderTypeDef rx_header;
		CAN_TxHeaderTypeDef tx_header;
	};
	uint8_t data[8];
} CAN_Message;

#define CAN_M_BUFFER_SIZE 2048

static uint8_t available[0x1000];
static CAN_Message buffer[0x1000];
static CAN_Message msgRec;

uint8_t CAN_API_Receive(uint16_t id, uint8_t* data)
{
	if(!available[id])
		return 0;

	available[id] = 0;
	uint8_t ret = 0;

	CAN_Message* msg = &buffer[id];

	ret = msg->rx_header.DLC;
	for(int i = 0;i < ret;i++) {
		data[i] = msg->data[i];
	}

	return ret;
}

uint8_t CAN_API_Send(CAN_HandleTypeDef *hcan, uint16_t id, uint8_t* data, uint8_t size)
{
	while(!HAL_CAN_GetTxMailboxesFreeLevel(hcan));
	CAN_TxHeaderTypeDef header;
	header.StdId = id;
	header.ExtId = id;
//	header.DLC = size;
	header.DLC = 8;
	header.RTR = CAN_RTR_DATA;
	header.IDE = CAN_ID_STD;
	uint32_t mbx;
	HAL_CAN_AddTxMessage(hcan, &header, data, &mbx);

	return 0;
}

uint8_t CAN_API_config_filter(CAN_HandleTypeDef* hcan,uint32_t maskLow, uint32_t maskHigh)
{
	CAN_FilterTypeDef filter;
	filter.FilterIdHigh = 0x02 << 5;
	filter.FilterIdLow = 0;
	filter.FilterMaskIdHigh = 0x1FE << 5;
	filter.FilterMaskIdLow = 0;
	filter.FilterFIFOAssignment = CAN_RX_FIFO0;
	filter.FilterBank = 0;//15;
	filter.FilterMode = CAN_FILTERMODE_IDMASK;
	filter.FilterScale = CAN_FILTERSCALE_32BIT;
	filter.FilterActivation = CAN_FILTER_ENABLE;

	HAL_CAN_ConfigFilter(hcan, &filter);

	return 0;
}

void CAN_API_activate_interrupt(CAN_HandleTypeDef* hcan)
{
	HAL_CAN_ActivateNotification(hcan, CAN_IT_RX_FIFO0_MSG_PENDING);
}

void printCanStatus()
{
	int cnt_valid = 0;
	for(int i = MIN_ID; i < MAX_ID;i++)
	{
		if(available[i])
		{
			cnt_valid++;
			//print something
		}
	}
}

void putInBufferSameID(CAN_Message* canMsg)
{
	uint32_t id = canMsg->rx_header.StdId;
	available[id] = 1;

	buffer[id].rx_header = canMsg->rx_header;
	int cnt = canMsg->rx_header.DLC;
	for(int i = 0; i < cnt;i++)
	{
		buffer[id].data[i] = canMsg->data[i];
	}
}

void can_irq(CAN_HandleTypeDef *hcan)
{
	if(HAL_CAN_GetRxMessage(hcan, 0, &msgRec.rx_header, msgRec.data) == HAL_OK)
	{
		putInBufferSameID(&msgRec);
	}
	printCanStatus();
}

uint8_t CAN_API_Start(CAN_HandleTypeDef* hcan)
{
	HAL_CAN_RegisterCallback(hcan, HAL_CAN_RX_FIFO0_MSG_PENDING_CB_ID, can_irq);
	HAL_CAN_Start(hcan);
	HAL_CAN_ActivateNotification(hcan, CAN_IT_RX_FIFO0_MSG_PENDING);

	return 0;
}

uint8_t CAN_API_Send_Long(CAN_HandleTypeDef *hcan, uint16_t id, uint64_t number)
{
	return CAN_API_Send(hcan, id, (uint8_t*)&number, 8);
}



