#include "CanParameters.h"
#include "canMessage.h"
//#include "FaultDetection.h"
#include "SubsystemsConfig.h"
//#include "Utils.h"
#include "can.h"
#include "can_api.h"

extern bool PumpVentCommReceived;
bool ECUGeneralStateReceived = false;
bool ECUPumpVentFirstMsgReceived = false;
bool shutdownRequest = false;
//CAN can1(PA_11, PA_12, CAN_BAUDRATE);
//CAN_Message message1;
CAN_Message pumpVentMsg;
CAN_Message ECUGeneralStateMsg;
BYTE VOLTAGE_INFO[8] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
BYTE TEMPERATURE_INFO[8] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
BYTE GENERAL_STATE[8] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
BYTE FAULTS[8] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};


/*void sendCANbyte(BYTE ID, BYTE data)
{
#ifdef SERIAL_HUMAN_READABLE
    pc1.printf("\nCAN SLANJE %x \n", data);
#endif
    message1.id = ID;
    message1.data[0] = data;
    can1.write(message1);
}*/



/*void sendCAN(int ID, unsigned char data[8])
{
    message1.id = ID;
    for (int i = 0; i < 8; i++)
    {
        message1.data[i] = data[i];
    }
    can1.write(message1);
}*/

// CAN RX Interrupt Function
void CANMsgReceive()
{
	pumpVentMsg = CAN_API_Receive_CAN_Message(ECU_COOLING_INFO);
	if(pumpVentMsg)
	{
		PumpVentCommReceived = true;
		turnOnActuatorNumber = 0;
	}
	ECUGeneralStateMsg = CAN_API_Receive_CAN_Message(ECU_GENERAL_STATE);
	if(ECUGeneralStateMsg)
	{
		ECUGeneralStateReceived = true;
	}
	uint8_t tmp;
	if(CAN_API_Receive(HV_FAULTS, &tmp))
	{
		if (tmp[RELAY_FAULTS] & 0x10)
		{
			shutdownRequest = true;
		}
	}
}

void ResetCANMessageBuffers()
{
    int k = 0;
    for (; k<4; k++)
    {
        VOLTAGE_INFO[k] = 0x00;
        TEMPERATURE_INFO[k] = 0x00;
    }
    TEMPERATURE_INFO[k++] = 0x00;
    TEMPERATURE_INFO[k++] = 0x00;
}

