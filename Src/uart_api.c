#include <uart_api.h>

int busy = 0;
int isDataValid[10] = {0};
int isTransmitComplete[10] = {0};
volatile uint8_t recieveBuffer[64];

#define UART_PC huart7

char output[256];

HAL_StatusTypeDef UART_PC_Send(const char* fmt, ...) {
	va_list va;
	va_start (va, fmt);
	HAL_StatusTypeDef status = HAL_UART_Transmit(&UART_PC, (unsigned char *)output, vsprintf(output, fmt, va), 10000);
	va_end (va);
	return status;
}

HAL_StatusTypeDef Uart_Recieve(UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size, uint32_t Timeout) {

	return HAL_UART_Receive(huart, pData, Size, Timeout);

}

HAL_StatusTypeDef Uart_Transmit(UART_HandleTypeDef *huart, const uint8_t *pData, uint16_t Size, uint32_t Timeout) {

	return HAL_UART_Transmit(huart, pData, Size, Timeout);

}

HAL_StatusTypeDef Uart_Recieve_IT(UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size) {

	if(huart->Instance == USART1) {
		isDataValid[0] = 0;
	}
	if(huart->Instance == USART2) {
		isDataValid[1] = 0;
	}
	if(huart->Instance == USART3) {
		isDataValid[2] = 0;
	}
	if(huart->Instance == UART4) {
		isDataValid[3] = 0;
	}
	if(huart->Instance == UART5) {
		isDataValid[4] = 0;
	}

	return HAL_UART_Receive_IT(huart, pData, Size);

}

HAL_StatusTypeDef Uart_Transmit_IT(UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size) {

	if(huart->Instance == USART1) {
		isTransmitComplete[0] = 0;
	}
	if(huart->Instance == USART2) {
		isTransmitComplete[1] = 0;
	}
	if(huart->Instance == USART3) {
		isTransmitComplete[2] = 0;
	}
	if(huart->Instance == UART4) {
		isTransmitComplete[3] = 0;
	}
	if(huart->Instance == UART5) {
		isTransmitComplete[4] = 0;
	}

	return HAL_UART_Transmit_IT(huart, pData, Size);

}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {

	Uart_Transmit(huart, "Usao u prekid", strlen("Usao u prekid"), 10000);

	if(huart->Instance == USART1) {
		isDataValid[0] = 1;
	}
	if(huart->Instance == USART2) {
		isDataValid[1] = 1;
	}
	if(huart->Instance == USART3) {
		isDataValid[2] = 1;
	}
	if(huart->Instance == UART4) {
		isDataValid[3] = 1;
	}
	if(huart->Instance == UART5) {
		isDataValid[4] = 1;
	}

}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart) {

	if(huart->Instance == USART1) {
		isTransmitComplete[0] = 1;
	}
	if(huart->Instance == USART2) {
		isTransmitComplete[1] = 1;
	}
	if(huart->Instance == USART3) {
		isTransmitComplete[2] = 1;
	}
	if(huart->Instance == UART4) {
		isTransmitComplete[3] = 1;
	}
	if(huart->Instance == UART5) {
		isTransmitComplete[4] = 1;
	}
}

int isDataReady(UART_HandleTypeDef *huart) {

	if(huart->Instance == USART1) {
		return isDataValid[0];
	}
	if(huart->Instance == USART2) {
		return isDataValid[1];
	}
	if(huart->Instance == USART3) {
		return isDataValid[2];
	}
	if(huart->Instance == UART4) {
		return isDataValid[3];
	}
	if(huart->Instance == UART5) {
		return isDataValid[4];
	}

}

int isTransmissionComplete(UART_HandleTypeDef *huart) { //MOGICE DA I TO TREBA DA SE DODA NISAM SIGURNA

	if(huart->Instance == USART1) {
		return isTransmitComplete[0];
	}
	if(huart->Instance == USART2) {
		return isTransmitComplete[1];
	}
	if(huart->Instance == USART3) {
		return isTransmitComplete[2];
	}
	if(huart->Instance == UART4) {
		return isTransmitComplete[3];
	}
	if(huart->Instance == UART5) {
		return isTransmitComplete[4];
	}

}


