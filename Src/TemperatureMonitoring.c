#include "TemperatureMonitoring.h"

volatile double Vin = 3.3;
volatile double Rref = 1000.0;
const double A = 0.003354016;
const double B = 0.000256524;
const double C = 0.00000260597;
const double D = 0.0000000632926;
const double R25 = 10000;
const double KelvinToCelzius = -272.15;

double TransferFunction(double R) //ovo je ta logaritamska fja za ntcove
{
    double res = A + B * log(R/R25) + C *  pow(log(R/R25), 2) + D * pow(log(R/R25), 3);
    return 1.0/res;
}
//Voltage Divider function may differ depending on schematic and the circuit itself, whether the NTC is connected to VCC or GND
//This voltage divider
int VoltageDivider(double Vout)  //NACI KAKO IZGLEDA KOLO ODAKLE SE DOBIJA OVA FJA AL SVAKAKO SE IZFLACI FJA IY OBLIKA KOLA
{
    double Rterm = Rref * (Vout/(Vin-Vout));
    double temperature = TransferFunction(Rterm);
    temperature = temperature + KelvinToCelzius;
    return temperature;
}


