#ifndef INC_ADC_API_H_
#define INC_ADC_API_H_

#include "stm32l4xx_hal.h"

uint32_t ADC_API_ReadValue_Sync(ADC_HandleTypeDef* hadc, uint32_t timeout);

uint8_t ADC_API_Start_Async_Conversion(ADC_HandleTypeDef* hadc, uint32_t* val);

uint32_t ADC_API_Read_Async(ADC_HandleTypeDef* hadc);


#endif /* INC_ADC_API_H_ */
