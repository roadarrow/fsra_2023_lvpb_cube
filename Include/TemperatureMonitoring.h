#ifndef INC_TEMPERATUREMONITORING_H_
#define INC_TEMPERATUREMONITORING_H_


extern volatile double Vin;
extern volatile double Rref;
extern char output[70];
extern SerialM pc1;

double TransferFunction(double R);
int VoltageDivider(double Vout);

#endif
