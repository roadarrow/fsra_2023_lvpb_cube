#ifndef INC_CAN_API_H_
#define INC_CAN_API_H_

#include "can.h"

/// @brief Uklanja iz bafera sve CAN poruke sa datim ID-jem i vraća najnoviju
/// @param
/// id - traženi ID CAN poruke
/// data - pokazivac u koji je potrebno upisati podatke
/// @returns Broj vracenih bajtova (0 ako nema poruka)
uint8_t CAN_API_Receive(uint16_t id, uint8_t* data);

/// @brief Salje CAN poruku na mrezu
/// @param
/// hcan - pokazivac na CAN komunikaciju
/// data - podaci koje je potrebno poslati
/// size - broj bajtova (velicina niza)
uint8_t CAN_API_Send(CAN_HandleTypeDef *hcan, uint16_t id, uint8_t* data, uint8_t size);

/// @brief postavlja filter koji propusta sve poruke datoj CAN komunikaciji i startuje CAN komunikaciju
/// @param
/// hcan - pokazivac na instancu CAN komunikacije
uint8_t CAN_API_config_filter(CAN_HandleTypeDef* hcan, uint32_t maskLow, uint32_t maskHigh);

/// @brief startuje datu CAN komunikaciju
/// @param
/// hcan - pokazivac na instancu CAN komunikacije
uint8_t CAN_API_Start(CAN_HandleTypeDef* hcan);

/// @brief salje long broj na CAN
/// @param
/// hcan - pokazivac na instancu CAN komunikacije
/// number - broj koji se salje
uint8_t CAN_API_Send_Long(CAN_HandleTypeDef *hcan, uint16_t id, uint64_t number);

#endif /* INC_CAN_API_H_ */
