#ifndef INC_CANMESSAGE_H_
#define INC_CANMESSAGE_H_


#include "ParametersConfig.h"

typedef unsigned char BYTE;

//extern SerialM pc1;
extern char output[70];

//extern CAN can1;
extern bool ECUGeneralStateReceived;
extern bool ECUGeneralStateReceivedFault;
extern bool ECUPumpVentFirstMsgReceived;
extern bool shutdownRequest;
//extern CANMessage message1;
//extern CANMessage pumpVentMsg;
//extern CANMessage ECUGeneralStateMsg;

extern BYTE VOLTAGE_INFO[8];
extern BYTE TEMPERATURE_INFO[8];
extern BYTE GENERAL_STATE[8];
extern BYTE FAULTS[8];

void sendCANbyte(BYTE ID, BYTE data);

void sendCAN(int ID, unsigned char data[8]);

void CANMsgReceive();

void ResetCANMessageBuffers();

#endif /* INC_CANMESSAGE_H_ */
