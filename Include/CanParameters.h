#ifndef INC_CANPARAMETERS_H_
#define INC_CANPARAMETERS_H_


#define DISPLAY_DRIVER_INPUT                    0x64
#define BTN_START                               0x00
#define SWITCH_ECO_SPORT                        0x01
#define SWITCH_TORQUE_ON_OFF                    0x02

#define HV_GENERAL_STATE                        0x65
#define GENERAL_STATE_HV_Periphery              0x00
#define GENERAL_STATE_HV_TotalVoltage_L         0x01
#define GENERAL_STATE_HV_TotalVoltage_H         0x02
#define GENERAL_STATE_HV_Current_L              0x03
#define GENERAL_STATE_HV_Current_H              0x04
#define GENERAL_STATE_HV_SOC                    0x05
#define GENERAL_STATE_HV_BoardFault_L           0x06
#define GENERAL_STATE_HV_BoardFault_H           0x06

#define LV_GENERAL_STATE                        0x66
#define BMS_LV_FAULTS                           0x00
#define ACC_ACTUATOR_FAULTS                     0x01
#define GENERAL_STATE_LV_TotalVoltageValue      0x02
#define GENERAL_STATE_LV_Current                0x03
#define CELL_CONN_AND_SAFETY                    0x04
#define GENERAL_STATE_LV_MaxCellTemperature     0x05
#define GENERAL_STATE_ACTUATOR                  0x06
#define GENERAL_STATE_LV_SOC                    0x07

#define DISPLAY_HEARTBEAT                       0x67
#define ALIVE                                   0x00

#define ECU_PRECHARGE_REQ                       0x97
#define PreCharge_Request                       0x00

#define HV_FAULTS                               0x98
#define RELAY_FAULTS                            0x00
#define PRECHARGE_FAULT_STATE                   0x01

#define HV_FAULT_0_L                            0x12c
#define BMS0_Fault_UV_L                         0x00
#define BMS0_Fault_UV_H                         0x01
#define BMS0_Fault_OV_L                         0x02
#define BMS0_Fault_OV_H                         0x03
#define BMS0_Fault_AUX_UV_OV_L                  0x04
#define BMS0_Fault_AUX_UV_OV_H                  0x05
#define BMS0_Fault_SYS                          0x07

#define HV_FAULT_0_H                            0x12d
#define BMS0_Fault_COMP_UV_L                    0x00
#define BMS0_Fault_COMP_UV_H                    0x01
#define BMS0_Fault_COMMN_L                      0x04
#define BMS0_Fault_COMMN_H                      0x05
#define BMS0_Fault_CHIP_L                       0x06
#define BMS0_Fault_CHIP_H                       0x07

#define HV_FAULT_1_L                            0x12e
#define BMS1_Fault_UV_L                         0x00
#define BMS1_Fault_UV_H                         0x01
#define BMS1_Fault_OV_L                         0x02
#define BMS1_Fault_OV_H                         0x03
#define BMS1_Fault_AUX_UV_OV_L                  0x04
#define BMS1_Fault_AUX_UV_OV_H                  0x05
#define BMS1_Fault_SYS                          0x07

#define HV_FAULT_1_H                            0x12f
#define BMS1_Fault_COMP_UV_L                    0x00
#define BMS1_Fault_COMP_UV_H                    0x01
#define BMS1_Fault_COMMN_L                      0x04
#define BMS1_Fault_COMMN_H                      0x05
#define BMS1_Fault_CHIP_L                       0x06
#define BMS1_Fault_CHIP_H                       0x07

#define HV_FAULT_2_L                            0x130
#define BMS2_Fault_UV_L                         0x00
#define BMS2_Fault_UV_H                         0x01
#define BMS2_Fault_OV_L                         0x02
#define BMS2_Fault_OV_H                         0x03
#define BMS2_Fault_AUX_UV_OV_L                  0x04
#define BMS2_Fault_AUX_UV_OV_H                  0x05
#define BMS2_Fault_SYS                          0x07

#define HV_FAULT_2_H                            0x131
#define BMS2_Fault_COMP_UV_L                    0x00
#define BMS2_Fault_COMP_UV_H                    0x01
#define BMS2_Fault_COMMN_L                      0x04
#define BMS2_Fault_COMMN_H                      0x05
#define BMS2_Fault_CHIP_L                       0x06
#define BMS2_Fault_CHIP_H                       0x07

#define HV_FAULT_3_L                            0x132
#define BMS3_Fault_UV_L                         0x00
#define BMS3_Fault_UV_H                         0x01
#define BMS3_Fault_OV_L                         0x02
#define BMS3_Fault_OV_H                         0x03
#define BMS3_Fault_AUX_UV_OV_L                  0x04
#define BMS3_Fault_AUX_UV_OV_H                  0x05
#define BMS3_Fault_SYS                          0x07

#define HV_FAULT_3_H                            0x133
#define BMS3_Fault_COMP_UV_L                    0x00
#define BMS3_Fault_COMP_UV_H                    0x01
#define BMS3_Fault_COMMN_L                      0x04
#define BMS3_Fault_COMMN_H                      0x05
#define BMS3_Fault_CHIP_L                       0x06
#define BMS3_Fault_CHIP_H                       0x07

#define HV_FAULT_4_L                            0x134
#define BMS4_Fault_UV_L                         0x00
#define BMS4_Fault_UV_H                         0x01
#define BMS4_Fault_OV_L                         0x02
#define BMS4_Fault_OV_H                         0x03
#define BMS4_Fault_AUX_UV_OV_L                  0x04
#define BMS4_Fault_AUX_UV_OV_H                  0x05
#define BMS4_Fault_SYS                          0x07

#define HV_FAULT_4_H                            0x135
#define BMS4_Fault_COMP_UV_L                    0x00
#define BMS4_Fault_COMP_UV_H                    0x01
#define BMS4_Fault_COMMN_L                      0x04
#define BMS4_Fault_COMMN_H                      0x05
#define BMS4_Fault_CHIP_L                       0x06
#define BMS4_Fault_CHIP_H                       0x07

#define HV_FAULT_5_L                            0x136
#define BMS5_Fault_UV_L                         0x00
#define BMS5_Fault_UV_H                         0x01
#define BMS5_Fault_OV_L                         0x02
#define BMS5_Fault_OV_H                         0x03
#define BMS5_Fault_AUX_UV_OV_L                  0x04
#define BMS5_Fault_AUX_UV_OV_H                  0x05
#define BMS5_Fault_SYS                          0x07

#define HV_FAULT_5_H                            0x137
#define BMS5_Fault_COMP_UV_L                    0x00
#define BMS5_Fault_COMP_UV_H                    0x01
#define BMS5_Fault_COMMN_L                      0x04
#define BMS5_Fault_COMMN_H                      0x05
#define BMS5_Fault_CHIP_L                       0x06
#define BMS5_Fault_CHIP_H                       0x07

#define HV_FAULT_6_L                            0x138
#define BMS6_Fault_UV_L                         0x00
#define BMS6_Fault_UV_H                         0x01
#define BMS6_Fault_OV_L                         0x02
#define BMS6_Fault_OV_H                         0x03
#define BMS6_Fault_AUX_UV_OV_L                  0x04
#define BMS6_Fault_AUX_UV_OV_H                  0x05
#define BMS6_Fault_SYS                          0x07

#define HV_FAULT_6_H                            0x139
#define BMS6_Fault_COMP_UV_L                    0x00
#define BMS6_Fault_COMP_UV_H                    0x01
#define BMS6_Fault_COMMN_L                      0x04
#define BMS6_Fault_COMMN_H                      0x05
#define BMS6_Fault_CHIP_L                       0x06
#define BMS6_Fault_CHIP_H                       0x07

#define HV_FAULT_7_L                            0x13a
#define BMS7_Fault_UV_L                         0x00
#define BMS7_Fault_UV_H                         0x01
#define BMS7_Fault_OV_L                         0x02
#define BMS7_Fault_OV_H                         0x03
#define BMS7_Fault_AUX_UV_OV_L                  0x04
#define BMS7_Fault_AUX_UV_OV_H                  0x05
#define BMS7_Fault_SYS                          0x07

#define HV_FAULT_7_H                            0x13b
#define BMS7_Fault_COMP_UV_L                    0x00
#define BMS7_Fault_COMP_UV_H                    0x01
#define BMS7_Fault_COMMN_L                      0x04
#define BMS7_Fault_COMMN_H                      0x05
#define BMS7_Fault_CHIP_L                       0x06
#define BMS7_Fault_CHIP_H                       0x07

#define HV_FAULT_8_L                            0x13c
#define BMS8_Fault_UV_L                         0x00
#define BMS8_Fault_UV_H                         0x01
#define BMS8_Fault_OV_L                         0x02
#define BMS8_Fault_OV_H                         0x03
#define BMS8_Fault_AUX_UV_OV_L                  0x04
#define BMS8_Fault_AUX_UV_OV_H                  0x05
#define BMS8_Fault_SYS                          0x07

#define HV_FAULT_8_H                            0x13d
#define BMS8_Fault_COMP_UV_L                    0x00
#define BMS8_Fault_COMP_UV_H                    0x01
#define BMS8_Fault_COMMN_L                      0x04
#define BMS8_Fault_COMMN_H                      0x05
#define BMS8_Fault_CHIP_L                       0x06
#define BMS8_Fault_CHIP_H                       0x07

#define HV_FAULT_9_L                            0x13e
#define BMS9_Fault_UV_L                         0x00
#define BMS9_Fault_UV_H                         0x01
#define BMS9_Fault_OV_L                         0x02
#define BMS9_Fault_OV_H                         0x03
#define BMS9_Fault_AUX_UV_OV_L                  0x04
#define BMS9_Fault_AUX_UV_OV_H                  0x05
#define BMS9_Fault_SYS                          0x07

#define HV_FAULT_9_H                            0x13f
#define BMS9_Fault_COMP_UV_L                    0x00
#define BMS9_Fault_COMP_UV_H                    0x01
#define BMS9_Fault_COMMN_L                      0x04
#define BMS9_Fault_COMMN_H                      0x05
#define BMS9_Fault_CHIP_L                       0x06
#define BMS9_Fault_CHIP_H                       0x07

#define HV_VOLTAGE_0_L                          0x15e
#define BMS0_CELL0_V                            0x00
#define BMS0_CELL1_V                            0x01
#define BMS0_CELL2_V                            0x02
#define BMS0_CELL3_V                            0x03
#define BMS0_CELL4_V                            0x04
#define BMS0_CELL5_V                            0x05
#define BMS0_CELL6_V                            0x06
#define BMS0_CELL7_V                            0x07

#define HV_VOLTAGE_0_H                          0x15f
#define BMS0_CELL8_V                            0x00
#define BMS0_CELL9_V                            0x01
#define BMS0_CELL10_V                           0x02
#define BMS0_CELL11_V                           0x03
#define BMS0_CELL12_V                           0x04
#define BMS0_CELL13_V                           0x05
#define BMS0_CELL14_V                           0x06

#define HV_VOLTAGE_1_L                          0x160
#define BMS1_CELL0_V                            0x00
#define BMS1_CELL1_V                            0x01
#define BMS1_CELL2_V                            0x02
#define BMS1_CELL3_V                            0x03
#define BMS1_CELL4_V                            0x04
#define BMS1_CELL5_V                            0x05
#define BMS1_CELL6_V                            0x06
#define BMS1_CELL7_V                            0x07

#define HV_VOLTAGE_1_H                          0x161
#define BMS1_CELL8_V                            0x00
#define BMS1_CELL9_V                            0x01
#define BMS1_CELL10_V                           0x02
#define BMS1_CELL11_V                           0x03
#define BMS1_CELL12_V                           0x04
#define BMS1_CELL13_V                           0x05
#define BMS1_CELL14_V                           0x06

#define HV_VOLTAGE_2_L                          0x162
#define BMS2_CELL0_V                            0x00
#define BMS2_CELL1_V                            0x01
#define BMS2_CELL2_V                            0x02
#define BMS2_CELL3_V                            0x03
#define BMS2_CELL4_V                            0x04
#define BMS2_CELL5_V                            0x05
#define BMS2_CELL6_V                            0x06
#define BMS2_CELL7_V                            0x07

#define HV_VOLTAGE_2_H                          0x163
#define BMS2_CELL8_V                            0x00
#define BMS2_CELL9_V                            0x01
#define BMS2_CELL10_V                           0x02
#define BMS2_CELL11_V                           0x03
#define BMS2_CELL12_V                           0x04
#define BMS2_CELL13_V                           0x05
#define BMS2_CELL14_V                           0x06

#define HV_VOLTAGE_3_L                          0x164
#define BMS3_CELL0_V                            0x00
#define BMS3_CELL1_V                            0x01
#define BMS3_CELL2_V                            0x02
#define BMS3_CELL3_V                            0x03
#define BMS3_CELL4_V                            0x04
#define BMS3_CELL5_V                            0x05
#define BMS3_CELL6_V                            0x06
#define BMS3_CELL7_V                            0x07

#define HV_VOLTAGE_3_H                          0x165
#define BMS3_CELL8_V                            0x00
#define BMS3_CELL9_V                            0x01
#define BMS3_CELL10_V                           0x02
#define BMS3_CELL11_V                           0x03
#define BMS3_CELL12_V                           0x04
#define BMS3_CELL13_V                           0x05
#define BMS3_CELL14_V                           0x06

#define HV_VOLTAGE_4_L                          0x166
#define BMS4_CELL0_V                            0x00
#define BMS4_CELL1_V                            0x01
#define BMS4_CELL2_V                            0x02
#define BMS4_CELL3_V                            0x03
#define BMS4_CELL4_V                            0x04
#define BMS4_CELL5_V                            0x05
#define BMS4_CELL6_V                            0x06
#define BMS4_CELL7_V                            0x07

#define HV_VOLTAGE_4_H                          0x167
#define BMS4_CELL8_V                            0x00
#define BMS4_CELL9_V                            0x01
#define BMS4_CELL10_V                           0x02
#define BMS4_CELL11_V                           0x03
#define BMS4_CELL12_V                           0x04
#define BMS4_CELL13_V                           0x05
#define BMS4_CELL14_V                           0x06

#define HV_VOLTAGE_5_L                          0x168
#define BMS5_CELL0_V                            0x00
#define BMS5_CELL1_V                            0x01
#define BMS5_CELL2_V                            0x02
#define BMS5_CELL3_V                            0x03
#define BMS5_CELL4_V                            0x04
#define BMS5_CELL5_V                            0x05
#define BMS5_CELL6_V                            0x06
#define BMS5_CELL7_V                            0x07

#define HV_VOLTAGE_5_H                          0x169
#define BMS5_CELL8_V                            0x00
#define BMS5_CELL9_V                            0x01
#define BMS5_CELL10_V                           0x02
#define BMS5_CELL11_V                           0x03
#define BMS5_CELL12_V                           0x04
#define BMS5_CELL13_V                           0x05
#define BMS5_CELL14_V                           0x06

#define HV_VOLTAGE_6_L                          0x16a
#define BMS6_CELL0_V                            0x00
#define BMS6_CELL1_V                            0x01
#define BMS6_CELL2_V                            0x02
#define BMS6_CELL3_V                            0x03
#define BMS6_CELL4_V                            0x04
#define BMS6_CELL5_V                            0x05
#define BMS6_CELL6_V                            0x06
#define BMS6_CELL7_V                            0x07

#define HV_VOLTAGE_6_H                          0x16b
#define BMS6_CELL8_V                            0x00
#define BMS6_CELL9_V                            0x01
#define BMS6_CELL10_V                           0x02
#define BMS6_CELL11_V                           0x03
#define BMS6_CELL12_V                           0x04
#define BMS6_CELL13_V                           0x05
#define BMS6_CELL14_V                           0x06

#define HV_VOLTAGE_7_L                          0x16c
#define BMS7_CELL0_V                            0x00
#define BMS7_CELL1_V                            0x01
#define BMS7_CELL2_V                            0x02
#define BMS7_CELL3_V                            0x03
#define BMS7_CELL4_V                            0x04
#define BMS7_CELL5_V                            0x05
#define BMS7_CELL6_V                            0x06
#define BMS7_CELL7_V                            0x07

#define HV_VOLTAGE_7_H                          0x16d
#define BMS7_CELL8_V                            0x00
#define BMS7_CELL9_V                            0x01
#define BMS7_CELL10_V                           0x02
#define BMS7_CELL11_V                           0x03
#define BMS7_CELL12_V                           0x04
#define BMS7_CELL13_V                           0x05
#define BMS7_CELL14_V                           0x06

#define HV_VOLTAGE_8_L                          0x16e
#define BMS8_CELL0_V                            0x00
#define BMS8_CELL1_V                            0x01
#define BMS8_CELL2_V                            0x02
#define BMS8_CELL3_V                            0x03
#define BMS8_CELL4_V                            0x04
#define BMS8_CELL5_V                            0x05
#define BMS8_CELL6_V                            0x06
#define BMS8_CELL7_V                            0x07

#define HV_VOLTAGE_8_H                          0x16f
#define BMS8_CELL8_V                            0x00
#define BMS8_CELL9_V                            0x01
#define BMS8_CELL10_V                           0x02
#define BMS8_CELL11_V                           0x03
#define BMS8_CELL12_V                           0x04
#define BMS8_CELL13_V                           0x05
#define BMS8_CELL14_V                           0x06

#define HV_VOLTAGE_9_L                          0x170
#define BMS9_CELL0_V                            0x00
#define BMS9_CELL1_V                            0x01
#define BMS9_CELL2_V                            0x02
#define BMS9_CELL3_V                            0x03
#define BMS9_CELL4_V                            0x04
#define BMS9_CELL5_V                            0x05
#define BMS9_CELL6_V                            0x06
#define BMS9_CELL7_V                            0x07

#define HV_VOLTAGE_9_H                          0x171
#define BMS9_CELL8_V                            0x00
#define BMS9_CELL9_V                            0x01
#define BMS9_CELL10_V                           0x02
#define BMS9_CELL11_V                           0x03
#define BMS9_CELL12_V                           0x04
#define BMS9_CELL13_V                           0x05
#define BMS9_CELL14_V                           0x06

#define HV_TEMP_0                               0x17c
#define BMS0_TEMP0_C                            0x00
#define BMS0_TEMP1_C                            0x01
#define BMS0_TEMP2_C                            0x02
#define BMS0_TEMP3_C                            0x03
#define BMS0_TEMP4_C                            0x04
#define BMS0_TEMP5_C                            0x05
#define BMS0_TEMP6_C                            0x06

#define HV_TEMP_1                               0x17d
#define BMS1_TEMP0_C                            0x00
#define BMS1_TEMP1_C                            0x01
#define BMS1_TEMP2_C                            0x02
#define BMS1_TEMP3_C                            0x03
#define BMS1_TEMP4_C                            0x04
#define BMS1_TEMP5_C                            0x05
#define BMS1_TEMP6_C                            0x06

#define HV_TEMP_2                               0x17e
#define BMS2_TEMP0_C                            0x00
#define BMS2_TEMP1_C                            0x01
#define BMS2_TEMP2_C                            0x02
#define BMS2_TEMP3_C                            0x03
#define BMS2_TEMP4_C                            0x04
#define BMS2_TEMP5_C                            0x05
#define BMS2_TEMP6_C                            0x06

#define HV_TEMP_3                               0x17f
#define BMS3_TEMP0_C                            0x00
#define BMS3_TEMP1_C                            0x01
#define BMS3_TEMP2_C                            0x02
#define BMS3_TEMP3_C                            0x03
#define BMS3_TEMP4_C                            0x04
#define BMS3_TEMP5_C                            0x05
#define BMS3_TEMP6_C                            0x06

#define HV_TEMP_4                               0x180
#define BMS4_TEMP0_C                            0x00
#define BMS4_TEMP1_C                            0x01
#define BMS4_TEMP2_C                            0x02
#define BMS4_TEMP3_C                            0x03
#define BMS4_TEMP4_C                            0x04
#define BMS4_TEMP5_C                            0x05
#define BMS4_TEMP6_C                            0x06

#define HV_TEMP_5                               0x181
#define BMS5_TEMP0_C                            0x00
#define BMS5_TEMP1_C                            0x01
#define BMS5_TEMP2_C                            0x02
#define BMS5_TEMP3_C                            0x03
#define BMS5_TEMP4_C                            0x04
#define BMS5_TEMP5_C                            0x05
#define BMS5_TEMP6_C                            0x06

#define HV_TEMP_6                               0x182
#define BMS6_TEMP0_C                            0x00
#define BMS6_TEMP1_C                            0x01
#define BMS6_TEMP2_C                            0x02
#define BMS6_TEMP3_C                            0x03
#define BMS6_TEMP4_C                            0x04
#define BMS6_TEMP5_C                            0x05
#define BMS6_TEMP6_C                            0x06

#define HV_TEMP_7                               0x183
#define BMS7_TEMP0_C                            0x00
#define BMS7_TEMP1_C                            0x01
#define BMS7_TEMP2_C                            0x02
#define BMS7_TEMP3_C                            0x03
#define BMS7_TEMP4_C                            0x04
#define BMS7_TEMP5_C                            0x05
#define BMS7_TEMP6_C                            0x06

#define HV_TEMP_8                               0x184
#define BMS8_TEMP0_C                            0x00
#define BMS8_TEMP1_C                            0x01
#define BMS8_TEMP2_C                            0x02
#define BMS8_TEMP3_C                            0x03
#define BMS8_TEMP4_C                            0x04
#define BMS8_TEMP5_C                            0x05
#define BMS8_TEMP6_C                            0x06

#define HV_TEMP_9                               0x185
#define BMS9_TEMP0_C                            0x00
#define BMS9_TEMP1_C                            0x01
#define BMS9_TEMP2_C                            0x02
#define BMS9_TEMP3_C                            0x03
#define BMS9_TEMP4_C                            0x04
#define BMS9_TEMP5_C                            0x05
#define BMS9_TEMP6_C                            0x06

#define INV_INFO_1_1                            0x11
#define Inverter1_AMK_Status_L                  0x00
#define Inverter1_AMK_Status_H                  0x01
#define Inverter1_AMK_ActualVelocity_L          0x02
#define Inverter1_AMK_ActualVelocity_H          0x03
#define Inverter1_Display_overload_invertor_L   0x04
#define Inverter1_Display_overload_invertor_H   0x05
#define Inverter1_Display_overload_motor_L      0x06
#define Inverter1_Display_overload_motor_H      0x07

#define INV_INFO_1_2                            0x12
#define Inverter2_AMK_Status_L                  0x00
#define Inverter2_AMK_Status_H                  0x01
#define Inverter2_AMK_ActualVelocity_L          0x02
#define Inverter2_AMK_ActualVelocity_H          0x03
#define Inverter2_Display_overload_invertor_L   0x04
#define Inverter2_Display_overload_invertor_H   0x05
#define Inverter2_Display_overload_motor_L      0x06
#define Inverter2_Display_overload_motor_H      0x07

#define INV_INFO_1_3                            0x13
#define Inverter3_AMK_Status_L                  0x00
#define Inverter3_AMK_Status_H                  0x01
#define Inverter3_AMK_ActualVelocity_L          0x02
#define Inverter3_AMK_ActualVelocity_H          0x03
#define Inverter3_Display_overload_invertor_L   0x04
#define Inverter3_Display_overload_invertor_H   0x05
#define Inverter3_Display_overload_motor_L      0x06
#define Inverter3_Display_overload_motor_H      0x07

#define INV_INFO_1_4                            0x14
#define Inverter4_AMK_Status_L                  0x00
#define Inverter4_AMK_Status_H                  0x01
#define Inverter4_AMK_ActualVelocity_L          0x02
#define Inverter4_AMK_ActualVelocity_H          0x03
#define Inverter4_Display_overload_invertor_L   0x04
#define Inverter4_Display_overload_invertor_H   0x05
#define Inverter4_Display_overload_motor_L      0x06
#define Inverter4_Display_overload_motor_H      0x07

#define INV_INFO_2_1                            0x21
#define Inverter1_AMK_TempMotor_L               0x00
#define Inverter1_AMK_TempMotor_H               0x01
#define Inverter1_AMK_TempInverter_L            0x02
#define Inverter1_AMK_TempInverter_H            0x03
#define Inverter1_AMK_TempIGBT_L                0x04
#define Inverter1_AMK_TempIGBT_H                0x05
#define Inverter1_ActualTorqueValue_L           0x06
#define Inverter1_ActualTorqueValue_H           0x07

#define INV_INFO_2_2                            0x22
#define Inverter2_AMK_TempMotor_L               0x00
#define Inverter2_AMK_TempMotor_H               0x01
#define Inverter2_AMK_TempInverter_L            0x02
#define Inverter2_AMK_TempInverter_H            0x03
#define Inverter2_AMK_TempIGBT_L                0x04
#define Inverter2_AMK_TempIGBT_H                0x05
#define Inverter2_ActualTorqueValue_L           0x06
#define Inverter2_ActualTorqueValue_H           0x07

#define INV_INFO_2_3                            0x23
#define Inverter3_AMK_TempMotor_L               0x00
#define Inverter3_AMK_TempMotor_H               0x01
#define Inverter3_AMK_TempInverter_L            0x02
#define Inverter3_AMK_TempInverter_H            0x03
#define Inverter3_AMK_TempIGBT_L                0x04
#define Inverter3_AMK_TempIGBT_H                0x05
#define Inverter3_ActualTorqueValue_L           0x06
#define Inverter3_ActualTorqueValue_H           0x07

#define INV_INFO_2_4                            0x24
#define Inverter4_AMK_TempMotor_L               0x00
#define Inverter4_AMK_TempMotor_H               0x01
#define Inverter4_AMK_TempInverter_L            0x02
#define Inverter4_AMK_TempInverter_H            0x03
#define Inverter4_AMK_TempIGBT_L                0x04
#define Inverter4_AMK_TempIGBT_H                0x05
#define Inverter4_ActualTorqueValue_L           0x06
#define Inverter4_ActualTorqueValue_H           0x07

#define ECU_MOTOR_CTRL_1                        0x1
#define Inverter1_AMK_Control_L                 0x00
#define Inverter1_AMK_Control_H                 0x01
#define Inverter1_AMK_TorqueSetpoint_L          0x02
#define Inverter1_AMK_TorqueSetpoint_H          0x03
#define Inverter1_AMK_TorqueLimitPositiv_L      0x04
#define Inverter1_AMK_TorqueLimitPositiv_H      0x05
#define Inverter1_AMK_TorqueLimitNegativ_L      0x06
#define Inverter1_AMK_TorqueLimitNegativ_H      0x07

#define ECU_MOTOR_CTRL_2                        0x2
#define Inverter2_AMK_Control_L                 0x00
#define Inverter2_AMK_Control_H                 0x01
#define Inverter2_AMK_TorqueSetpoint_L          0x02
#define Inverter2_AMK_TorqueSetpoint_H          0x03
#define Inverter2_AMK_TorqueLimitPositiv_L      0x04
#define Inverter2_AMK_TorqueLimitPositiv_H      0x05
#define Inverter2_AMK_TorqueLimitNegativ_L      0x06
#define Inverter2_AMK_TorqueLimitNegativ_H      0x07

#define ECU_MOTOR_CTRL_3                        0x3
#define Inverter3_AMK_Control_L                 0x00
#define Inverter3_AMK_Control_H                 0x01
#define Inverter3_AMK_TorqueSetpoint_L          0x02
#define Inverter3_AMK_TorqueSetpoint_H          0x03
#define Inverter3_AMK_TorqueLimitPositiv_L      0x04
#define Inverter3_AMK_TorqueLimitPositiv_H      0x05
#define Inverter3_AMK_TorqueLimitNegativ_L      0x06
#define Inverter3_AMK_TorqueLimitNegativ_H      0x07

#define ECU_MOTOR_CTRL_4                        0x4
#define Inverter4_AMK_Control_L                 0x00
#define Inverter4_AMK_Control_H                 0x01
#define Inverter4_AMK_TorqueSetpoint_L          0x02
#define Inverter4_AMK_TorqueSetpoint_H          0x03
#define Inverter4_AMK_TorqueLimitPositiv_L      0x04
#define Inverter4_AMK_TorqueLimitPositiv_H      0x05
#define Inverter4_AMK_TorqueLimitNegativ_L      0x06
#define Inverter4_AMK_TorqueLimitNegativ_H      0x07

#define ECU_STEER_INFO                          0x5
#define Steering_Whell_Angle_L                  0x00
#define Steering_Whell_Angle_H                  0x01

#define LV_VOLTAGE_INFO                       0x190
#define BMSLV_CELL0_V                           0x00
#define BMSLV_CELL1_V                           0x01
#define BMSLV_CELL2_V                           0x02
#define BMSLV_CELL3_V                           0x03
#define BMS_LV_UV_OV_Fault                      0x04
#define BMSLV_OVUV_BIST_FAULT                   0x05

#define LV_TEMP_INFO                          0x191
#define BMSLV_Temp0_V                           0x00
#define BMSLV_Temp1_V                           0x01
#define BMSLV_Temp2_V                           0x02
#define BMSLV_Temp3_V                           0x03
#define BMSLV_Temp4_V                           0x04
#define BMSLV_Temp5_V                           0x05
#define BMSLV_OT_Fault                          0x06
#define BMSLV_UT_Fault                          0x07

#define LV_FAULTS                             0x193
#define BMSLV_FaultSum_Reg                      0x00
#define BMSLV_SYS_FAULT1                        0x01
#define BMSLV_SYS_FAULT3                        0x02
#define BMSLV_COMM_UART_RC_FAULT                0x03
#define BMSLV_COMM_UART_RR_FAULT                0x04
#define BMSLV_OTUT_BIST_FAULT                   0x05
#define BMSLV_NTC_OC_Fault                      0x06
#define BMSLV_NTC_SC_Fault                      0x07

#define ECU_COOLING_INFO                        0x192
#define ON_Pumpe_Ventilatori                    0x00
#define PWM1                                    0x01
#define PWM2                                    0x02

#define ECU_GENERAL_STATE                       0x300
#define GENERAL_STATE_VEHICLE                   0x00
#define BMS_HV_state_OR_BMS_LV_state            0x01
#define APPS_state_OR_Inverter_state            0x02
#define Telemetry_Moduls_Status                 0x03


#endif
