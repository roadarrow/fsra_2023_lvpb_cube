#ifndef INC_PARAMETERSCONFIG_H_
#define INC_PARAMETERSCONFIG_H_

//-------GENERAL-------
#define TOTALBOARDS 1       //MUST SET: total boards in the stack
#define MAXBYTES 6*2        //6 CELLS, 2 BYTES EACH  ????STA JE OVO
//-------CAN----------
//#define CAN_BAUDRATE 500000 VEC POSTAVLJENO

//-------UART---------
//#define UART_BMS_BAUDRATE 1000000  PROVERI SA TIM STA CES OVO JE KAO ONA POCETNA VREDNOST PA SE POSLE NA 250K PREBACI AL PROVERI DAL TREBA ODMAH NA TU VREDNOST IL KAKO
//#define UART_PC_BAUDRATE 9600
#define UART_BMS_BAUDRATE_NEW 250000

//-------VOLTAGE MONITORING-------- NADJI STAJE SVE TACNO NEKA IH TU
//Cell voltage
#define UNDER_VOLTAGE 3.2
#define OVER_VOLTAGE 4.2
#define UV_DEBOUNCING_VAL 3
#define OV_DEBOUNCING_VAL 3
#define OPEN_WIRE 1
#define OW_DEBOUNCING_VAL 3
//-------CELL---------
#define NoOfCells 4
//Upper bound for detecting the shortcircuit
#define CELL_SHORTCIRCUIT 1
//Cell voltage imbalance beotween Umax and Umin
#define CELL_IMBALANCE 0.2

//------TEMPERATURE MONITORING----------
//Cell temperature
#define OVER_TEMPERATURE 60
#define UNDER_TEMPERATURE -20
#define OVER_TEMPERATURE_CHARGING 45
#define UNDER_TEMPERATURE_CHARGING 10
#define UT_DEBOUNCING_VAL 3
#define OT_DEBOUNCING_VAL 3
//Short circuit of one or more NTC sensors
#define NTC_SHORTCIRCUIT 0.5
//Open circuit of one or more NTC sensors
#define NTC_OPENCIRCUIT 2.6

#define NoOfNTCs 4
#define MinimumNoOfNTCsFunctional 0
#define NoOfNTCsFailureDebouncingVal 3
#define SHORT_CIRCUIT_NTC_ERROR_CODE 0xFF
#define OPEN_CIRCUIT_NTC_ERROR_CODE 0xFE


//-----------BALANCING-----------
#define BALANCING_TIME_PER_CELL 0x01      // in seconds (1 minute)
#define BALANCING_SEQUENCE      0xFF     // Odds then Evens, continue regardless of fault condition, 30sec, seconds
#define BALANCING_CELL_THRESHOLD  0x47    // Thresh hold set to value 3.0V, CBDONE comparators enabled
#define BALANCING_DUTY_CYCLE    30       // in seconds


//---------COOLING SYSTEM--------
#define noOfPumpFaults 8
#define noOfVentFaults 8
#define PUMP_SHORT_CIRCUT_THRESHOLD 9
#define PUMP_OPEN_CIRCUT_THRESHOLD 1
#define VENT_SHORT_CIRCUT_THRESHOLD 8
#define VENT_OPEN_CIRCUT_THRESHOLD 1
#define PUMP_OC_VAL 10
#define VENT_OC_VAL 7

#define ACTUATORS_CURRENT_DIFF_THRESHOLD 10


#define MAXIMUM_BUFFER_SIZE


//-----------TIMER------------
#define CHECK_TIMEOUT 2s
#define ADC_READ_FILTAR 10ms
#define INTEG_DT 0.01

//--------CURRENT SENSOR-------
#define MAIN_CURR_SENS_OC_VAL 35
#define MAIN_CURR_SENS_OC_DEBOUNCING_VAL 3
#define CS_ERROR_OFFSET 0.475

//---------24V-------------
#define ADC_24V_OV_VAL 3.15
#define ADC_24V_OV_DEBOUNCING_VAL 3

//---------CHARGING------------
#define MAX_DIFFERENCE_BETWEEN_CELL_VOLTAGES 0.005
#define MAX_CELL_VOLTAGE 4.15


#endif /* INC_PARAMETERSCONFIG_H_ */
