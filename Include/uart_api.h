#ifndef INC_UART_API_H_
#define INC_UART_API_H_

#include <stdint.h>
#include "stm32l4xx_hal.h"
#include <stdarg.h>

HAL_StatusTypeDef UART_PC_Send(const char* fmt, ...);

HAL_StatusTypeDef Uart_Recieve(UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size, uint32_t Timeout);

HAL_StatusTypeDef Uart_Recieve_IT(UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size);

HAL_StatusTypeDef Uart_Transmit(UART_HandleTypeDef *huart, const uint8_t *pData, uint16_t Size, uint32_t Timeout);

HAL_StatusTypeDef Uart_Transmit_IT(UART_HandleTypeDef *huart, uint8_t *pData, uint16_t Size);

int isDataReady(UART_HandleTypeDef *huart);

int isTransmissionComplete(UART_HandleTypeDef *huart);



#endif /* INC_UART_API_H_ */

